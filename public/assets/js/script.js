// windows on scroll
let btnScroll = document.querySelector('.btn-scroll_top');
window.onscroll = () =>{
	console.log(scrollY);
    if(scrollY >= 50){
       btnScroll.style.display = "block";
        // div1.classList.add("animate-1");
    } else if(scrollY <= 0 ){
    	btnScroll.style.display = "none";
    }
   	// if (scrollY >= 1100) {
    //   let div1 = document.querySelector('.p-content-1');
    //   let div2 = document.querySelector('.p-content-2');
    //   let div3 = document.querySelector('.p-content-3');
   	// 	div1.classList.add("animate__fadeInUp");
    //   div2.classList.add("animate__fadeInUp");
    //   div3.classList.add("animate__fadeInUp");
    //   div1.style.display = "block";
    //   div2.style.display = "block";
    //   div3.style.display = "block";
   	// }  
    // if (scrollY >= 1600){
    //   let rContent1 = document.querySelector('.resume-content-1');
    //   let rContent2 = document.querySelector('.resume-content-2');
    //   rContent1.classList.add("animate__fadeIn");
    //   rContent2.classList.add("animate__fadeIn");
    //   rContent1.style.display = "block";
    //   rContent2.style.display = "block";

    // }
}


// Owl-carousel
  $(document).ready(function(){
        $(".owl-carousel").owlCarousel();
      });
      var owl = $('.owl-carousel');
      owl.owlCarousel({
          items:5,
          loop:true,
          center:true,
          margin:10,
          autoWidth:false,
          autoplay:true,
          autoplayTimeout:3000,
          autoplayHoverPause:true,
          responsive:{
              0:{
                  items:1,
                  // nav:true
                  
              },
              400:{
                  items:3,
                  // nav:true
              },
              600:{
                  items:3,
                  // nav:false
              },
              1000:{
                  items:5,
                  // nav:true,
                  // loop:false
              }
          }
      });


  // typing Carousel

  var TxtRotate = function(el, toRotate, period) {
  this.toRotate = toRotate;
  this.el = el;
  this.loopNum = 0;
  this.period = parseInt(period, 10) || 2000;
  this.txt = '';
  this.tick();
  this.isDeleting = false;
};

TxtRotate.prototype.tick = function() {
  var i = this.loopNum % this.toRotate.length;
  var fullTxt = this.toRotate[i];

  if (this.isDeleting) {
    this.txt = fullTxt.substring(0, this.txt.length - 1);
  } else {
    this.txt = fullTxt.substring(0, this.txt.length + 1);
  }

  this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

  var that = this;
  var delta = 300 - Math.random() * 100;

  if (this.isDeleting) { delta /= 2; }

  if (!this.isDeleting && this.txt === fullTxt) {
    delta = this.period;
    this.isDeleting = true;
  } else if (this.isDeleting && this.txt === '') {
    this.isDeleting = false;
    this.loopNum++;
    delta = 500;
  }

  setTimeout(function() {
    that.tick();
  }, delta);
};

window.onload = function() {
  var elements = document.getElementsByClassName('txt-rotate');
  for (var i=0; i<elements.length; i++) {
    var toRotate = elements[i].getAttribute('data-rotate');
    var period = elements[i].getAttribute('data-period');
    if (toRotate) {
      new TxtRotate(elements[i], JSON.parse(toRotate), period);
    }
  }
  // INJECT CSS
  var css = document.createElement("style");
  css.type = "text/css";
  css.innerHTML = ".txt-rotate > .wrap { border-right: 0.08em solid #666; text-decoration: underline; }";
  document.body.appendChild(css);
};